<p>{{ __('You are receiving this email because we received a password reset request for your account.') }}</p>

<p>{{ __('Please enter code below in application to reset password:') }}</p>

<p><code>{{ $token }}</code></p>

<p>{{ __('If you did not request a password reset, no further action is required.') }}</p>