<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([            
            'role' => App\User::ROLES['ADMIN'],
            'email' => 'admin@email.com',
            'password' => 'secret',
        ]);
        factory(App\User::class)->create([            
            'email' => 'user@email.com',
            'password' => 'secret',
        ]);
        factory(App\User::class, 10)->create();
    }
}
