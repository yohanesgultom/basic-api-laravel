# Basic API Laravel

Basic REST API built with Laravel. 

* Demo: https://basic-api-laravel.herokuapp.com/
* Frontend/web-client: https://github.com/yohanesgultom/basic-admin-vuetify

Features:

* Basic OAuth2 API: register, verify email, login, logout, reset password
* API documentation with apidocjs.com
* Google Recaptcha V3 for login

## Installation

Prerequisites:

* PHP-FPM >= 7.2 (and [all modules](https://laravel.com/docs/6.x#server-requirements) required by Laravel)
* Composer >= 1.8.x
* SQLite >= 3.x or MySQL >= 5.7
* NodeJS >= 10.x and NPM >= 6.x (for API documentation)

Steps:

1. Copy `.env.example` to `.env` and adjust configuration
1. Install dependencies: `composer install` 
1. Generate secret key: `php artisan key:generate`
1. For SQLite, create database file manually `database/database.sqlite`
1. Migrate database and add demo data: `php artisan migrate:fresh --seed`
1. Install Passport: `php artisan passport:install`

On *nix OS, this is a one-line command:

```
composer install && php artisan key:generate && touch database/database.sqlite && php artisan migrate:fresh --seed && php artisan passport:install
```

## Running

Running in development mode:

1. Run `php artisan serve`
1. Open API doc in `http://localhost:8000`

## Testing

To run unit tests:

1. Create database for testing `database/database.test.sqlite`
1. Run testing `vendor/bin/phpunit`

## API Documentation

To generate API documentation using apidocjs.com:

1. Install dependencies `npm install`
1. Generate documentation `npm run doc`
1. Open documentation in `apidoc/index.html`
