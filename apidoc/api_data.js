define({ "api": [
  {
    "type": "post",
    "url": "/api/auth/login",
    "title": "Login",
    "name": "login",
    "group": "Authentication",
    "permission": [
      {
        "name": "public"
      }
    ],
    "version": "1.0.0",
    "description": "<p>Login with Google Recaptcha validation</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "g_recaptcha_response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"email\": \"user@email.com\",\n  \"password\": \"secret\",\n  \"g_recaptcha_response\": \"03AOL.....GSmc9XR\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "expires_in",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "refresh_token",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Example:",
          "content": "{\n  \"token_type\":\"Bearer\",\n  \"expires_in\":1296000,\n  \"access_token\":\"verylonggibberish\",\n  \"refresh_token\":\"anotherlonggibberish\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/LoginController.php",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/logout",
    "title": "Logout",
    "name": "logout",
    "group": "Authentication",
    "permission": [
      {
        "name": "authorized"
      }
    ],
    "version": "1.0.0",
    "description": "<p>Logout</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authentication",
            "description": "<p><code>Bearer {token}</code></p>"
          }
        ]
      }
    },
    "filename": "app/Http/Controllers/API/LoginController.php",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/register",
    "title": "Register",
    "name": "register",
    "group": "Authentication",
    "version": "1.0.0",
    "permission": [
      {
        "name": "public"
      }
    ],
    "description": "<p>Register with Google Recaptcha validation</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirmation",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "g_recaptcha_response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"email\": \"user@email.com\",\n  \"password\": \"secret\",\n  \"password_confirmation\": \"secret\",\n  \"g_recaptcha_response\": \"03AOL.....GSmc9XR\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Example:",
          "content": "{\n  \"email\":\"user@email.com\",\n  \"updated_at\":\"2020-01-21 13:39:30\",\n  \"created_at\":\"2020-01-21 13:39:30\",\n  \"id\":2,\n  \"verified\":false\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/RegisterController.php",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/resetPassword",
    "title": "ResetPassword",
    "name": "resetPassword",
    "group": "Authentication",
    "permission": [
      {
        "name": "public"
      }
    ],
    "version": "1.0.0",
    "description": "<p>Reset password using token sent to email</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_confirmation",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"email\": \"user@email.com\",\n  \"token\": \"tokensenttoemail\",\n  \"password\": \"newpassword\",\n  \"password_confirmation\": \"newpassword\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Example:",
          "content": "{\n  \"message\":\"Password has been changed\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/ResetPasswordController.php",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/sendResetPasswordToken",
    "title": "SendResetPasswordToken",
    "name": "sendResetPasswordToken",
    "group": "Authentication",
    "permission": [
      {
        "name": "public"
      }
    ],
    "version": "1.0.0",
    "description": "<p>Send reset password token to email with Google Recaptcha validation</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"email\": \"user@email.com\",\n  \"g_recaptcha_response\": \"03AOL.....GSmc9XR\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Example:",
          "content": "{\n  \"message\":\"Reset password token has been sent to your email\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/ResetPasswordController.php",
    "groupTitle": "Authentication"
  },
  {
    "type": "get",
    "url": "/api/auth/verify",
    "title": "Verify",
    "name": "verify",
    "group": "Authentication",
    "permission": [
      {
        "name": "public"
      }
    ],
    "version": "1.0.0",
    "description": "<p>Verify email</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Example:",
          "content": "{\n  \"message\": \"Email has been verified\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/VerificationController.php",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/user",
    "title": "User",
    "name": "user",
    "group": "User",
    "permission": [
      {
        "name": "authorized"
      }
    ],
    "version": "1.0.0",
    "description": "<p>Get logged-in user info</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "description": "<p><code>application/json</code></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Example:",
          "content": "{\n  \"id\":2,\n  \"email\":\"user@email.com\",\n  \"email_verified_at\":\"2020-01-21 13:41:16\",\n  \"created_at\":\"2020-01-21 13:39:30\",\n  \"updated_at\":\"2020-01-21 14:10:41\",\n  \"verified\":true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/API/UserController.php",
    "groupTitle": "User"
  }
] });
