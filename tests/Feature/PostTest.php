<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class PostTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();        
        $this->user = User::inRandomOrder()->first();
    }
    /**
     * Test index
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->json('GET', '/api/posts');
        $this->assertStatus(401, $response);
        $response = $this->actingAs($this->user, 'api')->json('GET', '/api/posts');
        $this->assertStatus(200, $response);
    }
}
