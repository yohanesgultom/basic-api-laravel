<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Mail;

class AuthTest extends TestCase
{
    public function testRegistration()
    {
        $client = \DB::table('oauth_clients')->where('password_client', 1)->first();

        // must not be able to access secured API
        $response = $this->json('GET', '/api/user');
        $response->assertStatus(401);

        // must be able to register
        Notification::fake();
        $user_data = [
            'email' => 'new_user@email.com',
            'password' => 'new_password',
        ];
        $response = $this->json('POST', '/api/auth/register', [
            'email' => $user_data['email'],
            'password' => $user_data['password'],
            'password_confirmation' => $user_data['password'],
        ]);        
        $response->assertStatus(200);
        $actual = $response->json();
        $this->assertEquals(false, $actual['verified']);

        // email must be sent
        $user = \App\User::find($actual['id']);
        $verify_qs = null;        
        Notification::assertSentTo(
            [$user], 
            \App\Notifications\VerifyEmailQueued::class,
            function ($notification, $channels) use ($user, &$verify_qs) {
                $data = $notification->toMail($user)->toArray();
                $url_data = parse_url($data['actionUrl']);
                $verify_qs = $url_data['query'];
                return true;
            }
        );
        
        // must be able to verify email
        $response = $this->json('GET', '/api/auth/verify?'.$verify_qs);
        $response->assertStatus(200);
        $user->refresh();
        $this->assertEquals(true, $user->verified);

        // must be able to login
        $response = $this->json('POST', '/api/auth/login', [
            'email' => $user_data['email'],
            'password' => $user_data['password'],
            'client_id' => $client->id,
            'client_secret' => $client->secret,
        ]);        
        $response->assertStatus(200);   
        $actual = $response->json();                
        
        // must not be able to access secured API
        $access_token = $actual['access_token'];
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer '.$access_token])
            ->json('GET', '/api/user');
        $response->assertStatus(200);
        $actual = $response->json();
        $this->assertEquals($user_data['email'], $actual['email']);

        // must be able to logout
        $response = $this
            ->withHeaders(['Authorization' => 'Bearer '.$access_token])
            ->json('POST', '/api/auth/logout');        
        $response->assertStatus(204);
        $user->refresh();
        $this->assertNull($user->token());
    }

    public function testLogout()
    {
        $client = \DB::table('oauth_clients')->where('password_client', 1)->first();

        $response = $this->json('POST', '/api/auth/logout');   
        $response->assertStatus(401);

        // must not be able to access secured API
        $user = \App\User::first();
        $response = $this
            ->actingAs($user, 'api')
            ->json('GET', '/api/user');
        $response->assertStatus(200);
    }

    public function testResetPassword()
    {
        $client = \DB::table('oauth_clients')->where('password_client', 1)->first();
        $user = \App\User::first();
        
        // must be able to request reset password link
        Mail::fake();
        $response = $this->json('POST', '/api/auth/sendResetPasswordToken', ['email' => $user->email]);
        $response->assertStatus(200);

        $token = null;   
        Mail::assertQueued(\App\Mail\EmailResetPassword::class, function ($mail) use ($user, &$token) {
            $token = $mail->token;
            return $user->id === $mail->user->id;
        });             

        // must be able to reset password
        $new_password = \Str::random(8);
        $params = [
            'token' => $token,
            'email' => $user->email,
            'password' => $new_password,
            'password_confirmation' => $new_password,
        ];
        $response = $this->json('POST', '/api/auth/resetPassword', $params);
        $response->assertStatus(200);
        $user->refresh(); 
        $this->assertTrue(\Hash::check($new_password, $user->password));       
    }
}
