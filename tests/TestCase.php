<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('db:seed');
        Artisan::call('passport:install');
    }

    public function assertStatus($status, $response)
    {
        if ($response->status() != $status) {
            $json = $response->json();  
            if (array_key_exists('message', $json)) {
                if (array_key_exists('file', $json)) print($json['file'].':');
                if (array_key_exists('line', $json)) print($json['line'].':');
                print($json['message']);                
            } else {
                print_r($json);
            }            
        }
        $response->assertStatus($status);
    }
}
