<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $tries = 3;

    public $token;
    public $user;    

    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    public function build()
    {   
        $app_name = config('app.name');
        $subject = __('Reset password Token');
        return $this            
            ->subject("[{$app_name}] {$subject}")
            ->view('email.emailResetPassword')
            ->with([
                'token' => $this->token,
            ]);
    }
}