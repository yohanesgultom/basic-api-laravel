<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Verified;

class VerificationController extends Controller
{
    use VerifiesEmails;

    /**
     * @api {get} /api/auth/verify Verify
     * @apiName verify
     * @apiGroup Authentication
	 * @apiPermission public
     * @apiVersion 1.0.0
     * @apiDescription Verify email
     *
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiSuccessExample {json} Success-Example:
     * {
     *   "message": "Email has been verified"
     * }
     */     
    public function verify(Request $request)
    {
        // check signature
        if (!$request->hasValidSignature()) {
            return $this->notAuthorized('Invalid signature');
        }
        
        // get user by id
        $user = \App\User::find($request->query('id'));
        if (empty($user)) {
            return $this->notFound('User not found');
        }

        // already verified
        if ($user->hasVerifiedEmail()) {
            return $this->forbidden('Already verified');
        }

        // verify
        if ($user->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }
 
        return response()->json(['message' => 'Email has been verified']);
    }    
}
