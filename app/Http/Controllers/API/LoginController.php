<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use App\User;

class LoginController extends Controller
{
    /**
     * @api {post} /api/auth/login Login
     * @apiName login
     * @apiGroup Authentication
     * @apiPermission public
     * @apiVersion 1.0.0
     * @apiDescription Login with Google Recaptcha validation
     *
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {String} email
     * @apiParam {String} password
     * @apiParam {String} g_recaptcha_response
     * 
     * @apiSuccess (Success 200) {Integer} expires_in
     * @apiSuccess (Success 200) {String} access_token 
     * @apiSuccess (Success 200) {String} refresh_token
     * 
     * @apiParamExample {json} Request-Example:
     * {
     *   "email": "user@email.com",
     *   "password": "secret",
     *   "g_recaptcha_response": "03AOL.....GSmc9XR"
     * }
     * 
     * @apiSuccessExample {json} Success-Example:
     * {
     *   "token_type":"Bearer",
     *   "expires_in":1296000,
     *   "access_token":"verylonggibberish",
     *   "refresh_token":"anotherlonggibberish"
     * }
     * 
     */    	
	public function login(Request $request)
	{
        $client = \DB::table('oauth_clients')->where('password_client', 1)->first();
        if (empty($client)) {
            return response()->json(['error' => 'no password_client found'], 500);
        }

        $rules = [
            'email' => 'required|email',
            'password' => 'required',            
        ];
        // if configured, force recaptcha
        if (!empty(config('app.recaptcha.site_key'))) {
            $rules['g_recaptcha_response'] = ['required', new \App\Rules\RecaptchaV3];
        }
        $request->validate($rules);

        // append fields
		$request->request->add([
			'username' => $request->input('email'),
            'grant_type' => 'password',
            'client_id' => $client->id,
			'client_secret' => $client->secret,
			'scope' => '*'
		]);

        // dispatch
        $tokenRequest = Request::create('/oauth/token','post');
        return Route::dispatch($tokenRequest);		
    }

    /**
     * @api {post} /api/auth/logout Logout
     * @apiName logout
     * @apiGroup Authentication
	 * @apiPermission authorized
     * @apiVersion 1.0.0
     * @apiDescription Logout
     *
     * @apiHeader {String} Accept `application/json`
     * @apiHeader {String} Authentication `Bearer {token}`
     * 
     */    	    
    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json(null, 204);
    }
}
