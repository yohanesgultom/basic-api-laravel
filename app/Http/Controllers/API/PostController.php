<?php

namespace App\Http\Controllers\API;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $order = $request->input('order', 'created_at');
        $sort = $request->input('limit', 'asc');
        $query = Post::whereNotNull('body');
        return response()->json($query->orderBy($order, $sort)->paginate($limit));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'body' => 'required',
        ]);
        $post = new Post;
        $post->fill($input);
        $post->user_id = \Auth::id();
        $post->save();
        return response()->json($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return response()->json($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {        
        $input = $request->validate([
            'body' => 'required',
        ]);
        if ($post->user_id != Auth::id()) {
            return response(null, 401);    
        } else {
            $post->fill($input);
            $post->user_id = Auth::id();
            $post->save();
            return response()->json($post);
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if ($post->user_id != Auth::id()) {
            return response(null, 401);    
        } else {
            $post->delete();
            return response(null, 204);    
        }
    }
}
