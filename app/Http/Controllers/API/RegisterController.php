<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Validator;
use App\User;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * @api {post} /api/auth/register Register
     * @apiName register
     * @apiGroup Authentication
     * @apiVersion 1.0.0
	 * @apiPermission public
     * @apiDescription Register with Google Recaptcha validation
     *
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {String} email
     * @apiParam {String} password
     * @apiParam {String} password_confirmation
     * @apiParam {String} g_recaptcha_response
     * 
     * @apiParamExample {json} Request-Example:
     * {
     *   "email": "user@email.com",
     *   "password": "secret",
     *   "password_confirmation": "secret",
     *   "g_recaptcha_response": "03AOL.....GSmc9XR"
     * }
     * 
     * @apiSuccessExample {json} Success-Example:
     * {
     *   "email":"user@email.com",
     *   "updated_at":"2020-01-21 13:39:30",
     *   "created_at":"2020-01-21 13:39:30",
     *   "id":2,
     *   "verified":false
     * }
     */      
    public function register(Request $request)
    {
        $input = $request->all();
        $input['role'] = \App\User::ROLES['USER'];
        $this->validator($input)->validate();
        event(new Registered($user = $this->create($input)));
        return response()->json($user->toArray());
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create($data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */    
    public function validator($input)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ];
        // if configured, force recaptcha
        if (!empty(config('app.recaptcha.site_key'))) {
            $rules['g_recaptcha_response'] = ['required', new \App\Rules\RecaptchaV3];
        }
        return Validator::make($input, $rules);
    }
}
