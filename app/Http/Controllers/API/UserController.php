<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**
     * @api {post} /api/user User
     * @apiName user
     * @apiGroup User
	 * @apiPermission authorized
     * @apiVersion 1.0.0
     * @apiDescription Get logged-in user info
     *
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiSuccess (Success 200) {Integer} id
     * @apiSuccess (Success 200) {String} email
     * 
     * @apiSuccessExample {json} Success-Example:
     * {
     *   "id":2,
     *   "email":"user@email.com",
     *   "email_verified_at":"2020-01-21 13:41:16",
     *   "created_at":"2020-01-21 13:39:30",
     *   "updated_at":"2020-01-21 14:10:41",
     *   "verified":true
     * }
     */    	    
    public function show(Request $request)
    {
        return $request->user();
    }
}
