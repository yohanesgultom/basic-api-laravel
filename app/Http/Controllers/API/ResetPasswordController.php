<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\EmailResetPassword;
use DB;
use Hash;
use Mail;
use Carbon\Carbon;
use App\User;

class ResetPasswordController extends Controller
{
    /**
     * @api {post} /api/auth/sendResetPasswordToken SendResetPasswordToken
     * @apiName sendResetPasswordToken
     * @apiGroup Authentication
	 * @apiPermission public
     * @apiVersion 1.0.0
     * @apiDescription Send reset password token to email with Google Recaptcha validation
     *
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {String} email
     * 
     * @apiSuccess (Success 200) {String} message
     * 
     * @apiParamExample {json} Request-Example:
     * {
     *   "email": "user@email.com",
     *   "g_recaptcha_response": "03AOL.....GSmc9XR"
     * }
     * 
     * @apiSuccessExample {json} Success-Example:
     * {
     *   "message":"Reset password token has been sent to your email"
     * }
     */    	
    public function sendResetPasswordToken(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);
        $email = $request->input('email');
        $user = User::where('email', $email)->first();
        if (empty($user)) {
            return $this->notFound(__('Email not found'));
        }
        $token = \Str::random(6);
        $password_reset = DB::table('password_resets')->updateOrInsert(
            ['email' => $email],
            ['token' => Hash::make($token), 'created_at' => Carbon::now()]
        );
        Mail::to($user->email)->queue(new EmailResetPassword($user, $token));
        return response()->json(['message' => __('Reset password token has been sent to your email')]);
    }

    /**
     * @api {post} /api/auth/resetPassword ResetPassword
     * @apiName resetPassword
     * @apiGroup Authentication
	 * @apiPermission public
     * @apiVersion 1.0.0
     * @apiDescription Reset password using token sent to email
     *
     * @apiHeader {String} Accept `application/json`
     * 
     * @apiParam {String} email
     * @apiParam {String} token
     * @apiParam {String} password
     * @apiParam {String} password_confirmation
     * 
     * @apiSuccess (Success 200) {String} message
     * 
     * @apiParamExample {json} Request-Example:
     * {
     *   "email": "user@email.com",
     *   "token": "tokensenttoemail",
     *   "password": "newpassword",
     *   "password_confirmation": "newpassword"
     * }
     * 
     * @apiSuccessExample {json} Success-Example:
     * {
     *   "message":"Password has been changed"
     * }
     */    
    public function resetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'token' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
        $input = $request->all();
        $password_reset = DB::table('password_resets')
            ->where('email', $input['email'])
            ->first();
        if (empty($password_reset)) {
            return $this->notFound('Email not found');
        }
        if (!Hash::check($input['token'], $password_reset->token)) {
            return $this->notFound('Invalid token');
        }
        $user = User::where('email', $password_reset->email)->first();
        if (empty($user)) {
            return $this->notFound('Email not found');
        }
        try {
            DB::beginTransaction();
            $user->password = $input['password'];
            $user->save();
            // invalidate access token
            $user->tokens()->delete();
            // delete password reset token
            DB::table('password_resets')->where('email', $password_reset->email)->delete();
            DB::commit();
            return response()->json(['message' => 'Password has been changed']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }    
}
