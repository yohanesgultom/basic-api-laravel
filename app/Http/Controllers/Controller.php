<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function notAuthorized($msg = 'Not authorized') {
        return response()->json(['error' => $msg], 401);
    }

    protected function notFound($msg = 'Data not found') {
        return response()->json(['error' => $msg], 404);
    }

    protected function forbidden($msg = 'Forbidden') {
        return response()->json(['error' => $msg], 403);
    }
    
    protected function badRequest($msg = 'Bad request') {
        return response()->json(['error' => $msg], 400);
    }

    protected function error($msg = 'Server error') {
        return response()->json(['error' => $msg], 500);
    }

}
