<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->middleware(['throttle:1,5'])->group(function () {
    Route::post('register', 'API\RegisterController@register');
    Route::post('sendResetPasswordToken', 'API\ResetPasswordController@sendResetPasswordToken');
});

Route::prefix('auth')->middleware(['throttle:60,1'])->group(function () {
    Route::post('login', 'API\LoginController@login')->name('login');
    Route::get('verify', 'API\VerificationController@verify')->name('verification.verify');
    Route::post('resetPassword', 'API\ResetPasswordController@resetPassword');
});

Route::middleware(['throttle:60,1', 'auth:api', 'verified'])->group(function () {
    Route::post('auth/logout', 'API\LoginController@logout');
    Route::get('user', 'API\UserController@show');
    Route::apiResource('posts', 'API\PostController');
});

// handle cors preflight
Route::options('{path}', function () {})->where('path', '.+');
