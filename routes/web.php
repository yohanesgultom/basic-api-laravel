<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/apidoc/{path?}', function ($path) {    
    $path = !empty($path) ? str_replace('..', '', $path) : 'index.html';
    return File::get(public_path().'/apidoc/'.$path);
});

Route::get('/', function () {    
    return redirect('apidoc/index.html');
});

